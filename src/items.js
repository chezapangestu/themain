export default [
	{
		name: 'Flower Tumblr',
		price: '50,000',
		img: '1.jpeg'
	},
	{
		name: 'Floral Tumblr',
		price: '50,000',
		img: '2.jpeg'
	},
	{
		name: 'Polka Tumblr',
		price: '100,000',
		img: '3.jpeg'
	},
	{
		name: 'Tumblr set',
		price: '500,000',
		img: '4.webp'
	},
	{
		name: 'Lock n Lock Tumblr',
		price: '80,000',
		img: '5.jpeg'
	},
	{
		name: 'Tumblr Hitam',
		price: '95,000',
		img: '6.jpeg'
	},
	{
		name: 'Tumblr tahan banting',
		price: '50,000',
		img: '7.jpeg'
	},
	{
		name: 'Tumblr anti pecah',
		price: '50,000',
		img: '8.jpeg'
	},
	{
		name: 'Botol ibu-ibu',
		price: '50,000',
		img: '9.webp'
	}
]
